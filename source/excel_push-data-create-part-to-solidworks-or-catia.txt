'Get values from excel cells to create Solidworks and Catia model based on ready templates
'Create template with given dimension names
'Change paths, excel cell locations

Private Sub CommandButton1_Click()
  
    Dim swApp, Part As Object
    Dim boolstatus As Boolean
    Dim longstatus As Long, longwarnings As Long
    
    Dim Dim_A, Dim_B, Dim_C, Dim_D, Dim_Lenght, strTemplate
    
    On Error Resume Next
    Set swApp = GetObject(, "SldWorks.Application")
    If swApp Is Nothing Then
    MsgBox "Please open SW and try again"
    Exit Sub
    End If
    swApp.Visible = True
    
    If InStr(Worksheets("Sayfa1").Range("B13"), "NAS1832") Then
        cellNum1 = 15
        strTemplate = "nas1832_template"
    ElseIf InStr(Worksheets("Sayfa1").Range("B13"), "NAS1834") Then
        cellNum1 = 16
        strTemplate = "nas1834_template"
    ElseIf InStr(Worksheets("Sayfa1").Range("B13"), "NAS1835") Then
        cellNum1 = 17
        strTemplate = "nas1835_template"
    ElseIf InStr(Worksheets("Sayfa1").Range("B13"), "NAS1836") Then
        cellNum1 = 18
        strTemplate = "nas1836_template"
    End If
    
    Dim_A = Worksheets("Sayfa1").Range("N" & cellNum1).Value
    Dim_B = Worksheets("Sayfa1").Range("O" & cellNum1).Value
    Dim_C = Worksheets("Sayfa1").Range("P" & cellNum1).Value
    Dim_D = Worksheets("Sayfa1").Range("Q" & cellNum1).Value
    Dim_Lenght = Worksheets("Sayfa1").Range("B14").Value
    If Dim_Lenght = "thru" Then Dim_Lenght = Worksheets("Sayfa1").Range("B8").Value
    
    If Dim_A = "N/A" Or Dim_B = "N/A" Or Dim_Lenght = "N/A Lenght" Then
        MsgBox "Some selections are not suitable, please check"
        Exit Sub
    End If
    
'prompt folder and copy template file then open copied file and modify
Dim returned As String
returned = copyTemplateFolder(strTemplate, True)
'MsgBox returned

If strTemplate <> "" Then
    Set Part = swApp.OpenDoc6(returned, 1, 0, "", longstatus, longwarnings)
    swApp.ActivateDoc2 strTemplate, False, longstatus
    Else
    MsgBox "Required template is not available"
    Exit Sub
End If
If returned <> "" Then
    Set Part = swApp.ActiveDoc
    Dim myDimension As Object
    Set myDimension = Part.Parameter("Dim_A@Sketch1")
    myDimension.SystemValue = Dim_A / 1000 * 25.4
    Set myDimension = Part.Parameter("Dim_B@Sketch1")
    myDimension.SystemValue = Dim_B / 1000 * 25.4
    Set myDimension = Part.Parameter("Dim_D@Sketch1")
    myDimension.SystemValue = Dim_D / 1000 * 25.4
    If cellNum1 = 17 Then
    Set myDimension = Part.Parameter("Dim_C@Sketch2")
    myDimension.SystemValue = Dim_C / 1000 * 25.4
    End If
    Set myDimension = Part.Parameter("Dim_L@Sketch1")
    myDimension.SystemValue = Dim_Lenght / 1000

    Part.ClearSelection2 True
    Part.ForceRebuild3 (False)
    boolstatus = Part.Extension.SelectByID2("Sketch1", "SKETCH", 0, 0, 0, False, 0, Nothing, 0)
    Part.ActivateSelectedFeature
    'Part.ViewZoomtofit2
    swApp.SendMsgToUser "It's done"
End If

End Sub

Private Function copyTemplateFolder(templateString, cad As Boolean) As String

Dim vbAnswer, source, dest, varResult, cadExtension
vbAnswer = MsgBox("Do you want to save your file?" & vbCrLf & "If so, select a folder and name for your part", vbQuestion + vbYesNo)
If cad Then cadExtension = ".SLDPRT" Else cadExtension = ".CATPart"

If vbAnswer = vbYes Then
    varResult = Application.GetSaveAsFilename(templateString & "_00_" & Environ("USERNAME"))
        If varResult <> False Then
        dest = varResult & Replace(cadExtension, ".", "")
        Else
        MsgBox "You cancelled"
        Exit Function
        End If
        source = "D:\Extension\computer aided design\vba_macro_cad-static\Insert_neri\" & templateString & cadExtension
        FileCopy source, dest
        copyTemplateFolder = dest
        Set diaFolder = Nothing
    Else
copyTemplateFolder = "D:\Extension\computer aided design\vba_macro_cad-static\Insert_neri\" & templateString & cadExtension
End If

End Function

Private Sub CommandButton2_Click()

    Dim Dim_A, Dim_B, Dim_C, Dim_D, Dim_Lenght, strTemplate
    
    On Error Resume Next
    Set catProg = GetObject(, "CATIA.Application")
    If catProg Is Nothing Then
    MsgBox "Please open CATIA and try again"
    Exit Sub
    End If
    catProg.Visible = True
    
    If InStr(Worksheets("Sayfa1").Range("B13"), "NAS1832") Then
        cellNum1 = 15
        strTemplate = "nas1832_template"
    ElseIf InStr(Worksheets("Sayfa1").Range("B13"), "NAS1834") Then
        cellNum1 = 16
        strTemplate = "nas1834_template"
    ElseIf InStr(Worksheets("Sayfa1").Range("B13"), "NAS1835") Then
        cellNum1 = 17
        strTemplate = "nas1835_template"
    ElseIf InStr(Worksheets("Sayfa1").Range("B13"), "NAS1836") Then
        cellNum1 = 18
        strTemplate = "nas1836_template"
    End If
    
    Dim_A = Worksheets("Sayfa1").Range("N" & cellNum1).Value
    Dim_B = Worksheets("Sayfa1").Range("O" & cellNum1).Value
    Dim_C = Worksheets("Sayfa1").Range("P" & cellNum1).Value
    Dim_D = Worksheets("Sayfa1").Range("Q" & cellNum1).Value
    Dim_Lenght = Worksheets("Sayfa1").Range("B14").Value
    If Dim_Lenght = "thru" Then Dim_Lenght = Worksheets("Sayfa1").Range("B8").Value
    
    If Dim_A = "N/A" Or Dim_B = "N/A" Or Dim_Lenght = "N/A Lenght" Then
        MsgBox "Some selections are not suitable, please check"
        Exit Sub
    End If
    
    Dim returned As String
    returned = copyTemplateFolder(strTemplate, False)
    
    If strTemplate <> "" Then
        Dim partDocument1 As PartDocument
        Set partDocument1 = catProg.Documents.Open(returned)
        'Set partDocument1 = catProg.ActiveDocument
    Else
        MsgBox "Required template is not available"
        Exit Sub
    End If
    
    If returned <> "" Then
    partDocument1.Part.Bodies.Item("PartBody").Sketches.Item("Sketch.1").Constraints.Item("Dim_A").Dimension.Value = Dim_A * 25.4
    partDocument1.Part.Bodies.Item("PartBody").Sketches.Item("Sketch.1").Constraints.Item("Dim_B").Dimension.Value = Dim_B * 25.4
    'partDocument1.Part.Bodies.Item("PartBody").Sketches.Item("Sketch.1").Constraints.Item("Dim_C").Dimension.Value = Dim_C * 25.4
    partDocument1.Part.Bodies.Item("PartBody").Sketches.Item("Sketch.1").Constraints.Item("Dim_D").Dimension.Value = Dim_D * 25.4
    partDocument1.Part.Bodies.Item("PartBody").Sketches.Item("Sketch.1").Constraints.Item("Dim_L").Dimension.Value = Dim_Lenght
        If cellNum = 18 Then
            If Dim_A < 18 Then
        partDocument1.Part.Bodies.Item("PartBody").Sketches.Item("Sketch.1").Constraints.Item("Offset.52").Dimension.Value = _
        ((Dim_A * 25.4) - (Dim_B * 25.4)) / 2
            Else
        partDocument1.Part.Bodies.Item("PartBody").Sketches.Item("Sketch.1").Constraints.Item("Offset.52").Dimension.Value = _
        ((Dim_A * 25.4) - (Dim_B * 25.4)) * 3
            End If
        End If
    partDocument1.Part.Update
    End If
End Sub
