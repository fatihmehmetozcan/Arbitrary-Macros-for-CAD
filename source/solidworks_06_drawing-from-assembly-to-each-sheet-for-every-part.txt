'https://forum.solidworks.com/thread/214253

Option Explicit
Dim swApp As SldWorks.SldWorks
Sub main()

Set swApp = Application.SldWorks
Dim swmodel As SldWorks.ModelDoc2
Set swmodel = swApp.ActiveDoc
Dim swext As SldWorks.ModelDocExtension
Set swext = swmodel.Extension

If swmodel Is Nothing Then
' If no model is currently loaded, then exit
Exit Sub
ElseIf (swmodel.GetType <> swDocASSEMBLY) Then
swApp.SendMsgToUser ("Open an assembly please")
Exit Sub
End If


Dim swcomponent As SldWorks.Component2
Dim swdraw As DrawingDoc
Dim swassy As AssemblyDoc
Dim com As Variant
Dim templatepath As String
Dim assypathlist() As Variant

Dim k As Integer
Dim i As Integer
Dim composcount As Integer

Set swassy = swmodel
composcount = swassy.GetComponentCount(True)

For Each com In swassy.GetComponents(True)
    k = k + 1
    'MsgBox com.Name
    ReDim Preserve assypathlist(1 To k)
    assypathlist(k) = com.GetPathName
    'MsgBox assypathlist(k)
Next com


Dim shcount As Variant
Dim sh As SldWorks.Sheet
Dim j As Integer
Dim boolstatus As Boolean

templatepath = "D:\Extension\computer aided design\denemeler\forum\macro\forum drawing from assembly for every part view\Draw-template.slddrw"
Set swdraw = swApp.NewDocument(templatepath, 0, 0, 0)
Set swdraw = swApp.ActiveDoc
Set sh = swdraw.GetCurrentSheet
swext.ViewZoomToSheet

Dim sheetheight As Double
Dim sheetwidth As Double
Dim longwar As Long
sh.GetSize sheetwidth, sheetheight

'checking automatic scale setting in solidworks system options, disabling if it's on
Dim checksetting As Boolean
If swApp.GetUserPreferenceToggle(swAutomaticScaling3ViewDrawings) = True Then
checksetting = True
swApp.SetUserPreferenceToggle swUserPreferenceToggle_e.swAutomaticScaling3ViewDrawings, False
End If

'swdraw.Create3rdAngleViews2 assypathlist(LBound(assypathlist))

'creates isometric view to specified location
swdraw.CreateDrawViewFromModelView3 assypathlist(LBound(assypathlist)), "*Isometric", sheetwidth - 0.05, sheetheight - 0.05, 0

shcount = swdraw.GetSheetCount
For j = shcount To composcount - 1
    'creates a new sheet and inserts views
    boolstatus = swdraw.NewSheet4("Sheet" & j + 1, swDwgPaperA3size, swDwgTemplateCustom, 1, 1, False, Environ("userprofile") + "\Desktop\a3 - landscape.slddrt", 0#, 0#, 0#, 0#, 0#, 0#, 0#, 0#, 0#)
swext.ViewZoomToSheet
    'swdraw.Create3rdAngleViews2 assypathlist(j + 1)
    swdraw.CreateDrawViewFromModelView3 assypathlist(j + 1), "*Isometric", sheetwidth - 0.05, sheetheight - 0.05, 0
Next j

'set automatic scale option true if it was on at the beginning
If checksetting = True Then
swApp.SetUserPreferenceToggle swUserPreferenceToggle_e.swAutomaticScaling3ViewDrawings, True
End If

End Sub
